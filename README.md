# Zoe - Advance Programming Project B3

## Coverage Report
Setiap push di masing2 branch bisa liat code coverage di stage 'test' <br>
Path for Test Coverage with HTML format: <br>
&nbsp;&nbsp; - Click on the latest commit <br> 
&nbsp;&nbsp; - From pipelines, choose 'test' <br>
&nbsp;&nbsp; - On the right side, you'll see 'Job Artifacts', then click browse or download <br>
&nbsp;&nbsp; - Click the public dir. , then click index.html
## Project Description
Zoe is a Line Chatbot dedicated to connect its users with Canteen Counters 
so that the users are able to order menus from Zoe instead off ordering manually to the counter.
It is an app that is able to be used by both buyers and counter sellers.

Users are able to order menus from Zoe whereas counter sellers are able to
put their menu in Zoe's menu list and when a user is ordering a menu to the counter
the counter will get a notification of that order.

Hopefully Zoe may be able to satisfy your needs!

## Project Developers

- Raihansyah Attallah Andrian (1706040196)
- Daniel Anderson Estefan (1706039641)
- Dimas Aditya Faiz (1706039686)
- Fanisya Dewi N. (1706039894)
- Stefanus Khrisna Aji Hardiyanto (1706074921)

## Claims

As developers, we hope that Zoe may satisfy both buyers and sellers needs.
We are very sorry if there are any inconvenience when using our chatbot Zoe.

If you wish to report any bugs on our bot that you find please contact
one of our developers immediately.

## Project Task Checklist

### Week 1

- [x] Week 1: Create Users class and its implementation (Raihansyah Attallah A)
- [x] Week 1: Create tests for Users class 
(Raihansyah Attallah A)
- [x] Week 1: Create Pembeli class and Counter class 
implementation (Fanisya Dewi)
- [x] Week 1: Create tests for Pembeli, and Counter class (Fanisya Dewi)
- [x] Week 1: Create Command Interface in Commands app (Dimas Aditya)
- [x] Week 1: Create HelpCommand Class in Commands app (Dimas Aditya)
- [x] Week 1: Create tests for Command (Dimas Aditya)
- [x] Week 1: Create Invoker class in Invoker app (Stefanus Khrisna)
- [x] Week 1: Create tests for Invoker (Stefanus Khrisna)
- [x] Week 1: Create Menu class in Menus app (Daniel Anderson)
- [x] Week 1: Create tests for Menus (Daniel Anderson)

### Week 2

- [x] Week 2: Tests for ZoeReceiver class [Raihansyah Attallah]
- [x] Week 2: Implement ZoeReceiver class [Raihansyah Attallah]

Create in Receiver app.

- [x] Week 2: Tests for HelpZoeReceiver class [Dimas Aditya]
- [x] Week 2: Implement HelpZoeReceiver class [Dimas Aditya]

Create in Receiver app.

- [x] Week 2: Test for Controller class [Daniel Anderson]
- [x] Week 2: Create Controller Class implementations 
[Daniel Anderson]

Create in Main.

- [x] Week 2: Test for Zoe class (if needed) [Daniel Anderson]
- [x] Week 2: Implement ZoeMain class in Zoe [Daniel Anderson]

Create in Main.

- [x] Week 2: Test for the new list of Users holder class (UsersList)
[Fanisya Dewi]
- [x] Week 2: Implement the class that holds Users (UsersList)
[Fanisya Dewi]

Create in Receiver app.

- [x] Week 2: Test for the DaftarCommand Class [Stefanus Khrisna]
- [x] Week 2: Implement the DaftarCommand Class [Stefanus Khrisna]

Create in Commands app.

### Week 3

- [x] Week 3: Refactoring week 2 implementations [Raihansyah Attallah A]
- [x] Week 3: Test for ZoeIterator interface [Dimas Aditya]
- [x] Week 3: Implement ZoeIterator interface [Dimas Aditya]

Iterator App.

- [x] Week 3: Test for DaftarCommand implementation in main [Daniel Anderson]
- [x] Week 3: Implement DaftarCommand in main [Daniel Anderson]

ZoeController & Zoe Class.

- [x] Week 3: Test for MenuZoeIterator class [Stefanus Khrisna]
- [x] Week 3: Implement MenuZoeIterator class [Stefanus Khrisna]

Iterator App.

- [ ] Week 3: Test for Waitress class [Raihansyah Attallah A]
- [x] Week 3: Implement Waitress class [Raihansyah Attallah A]

Receiver App.

- [x] Week 3: Test for TambahMenuCommand class [Dimas Aditya]
- [x] Week 3: Implement TambahMenuCommand class [Dimas Aditya]

Command App.

- [x] Week 3: Test for CreateIterator Method in Counter [Fanisya Dewi]
- [x] Week 3: Implement for CreateIterator Method in Counter [Fanisya Dewi]

Users App.

- [x] Week 3: Test for MenuZoeCommand class [Fanisya Dewi]
- [x] Week 3: Implement for MenuZoeCommand class [Fanisya Dewi]

Command App.

### Week 4 - Target: Able to Print List Counter and List Pembeli

- [x] Week 4: Refactoring week 3 implementations 
[Raihansyah Attallah A] [Daniel Anderson]
- [x] Week 4: Test UsersListIterator class. [Stefanus Khrisna]
- [x] Week 4: Create UsersListIterator class. [Stefanus Khrisna]

In Iterator app.

- [ ] Week 4: Test UsersListWaitress Receiver. [Raihansyah Attallah A]
- [ ] Week 4: Create UsersListWaitress Receiver. [Raihansyah Attallah A]

In Receiver app.

- [x] Week 4: Test for ListOfCounter command class. [Dimas Aditya]
- [x] Week 4: Create ListOfCounter command class. [Dimas Aditya]

- [x] Week 4: Test for ListOfPembeli command class. [Dimas Aditya]
- [x] Week 4: Create ListOfPembeli command class. [Dimas Aditya]

In Command app.

- [ ] Week 4: Test the new implementation in controller. 
[Daniel Anderson] [Fanisya Dewi]
- [ ] Week 4: Implement the new Commands in controller. 
[Daniel Anderson] [Fanisya Dewi]

In Main app.

- [ ] Week 4: Test the ZoeMain class (if needed) [All]
- [ ] Week 4: Implement the new classes in ZoeMain [All]

Main App.

## My Notes - Project

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1. Command yang ada di Zoe:
- HELP (help command)
- DAFTAR [nama_user] [jenis_user] (daftar command)
- TAMBAH_MENU [Nama_Counter] [Nama_Menu] [Harga_Satuan] 
(tambah menu command)
- MENU (menu command)
- PESAN [Nama_Counter] [No_Menu] [Jumlah]
- LIST_COUNTER (list counter command)
- LIST_PEMBELI (list pembeli command)