package com.botline.zoe.menus;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenusTest {
    private MenuItem menus;

    @Before
    public void setUp() {
        menus = new MenuItem("menutest", 1000);
    }

    @Test
    public void testGetName() {
        assertEquals(menus.getNama(), "menutest");
    }

    @Test
    public void testSetName() {
        menus.setNama("menubaru");
        assertEquals(menus.getNama(), "menubaru");
    }

    @Test
    public void testGetHarga() {
        assertEquals(menus.getHarga(), 1000);
    }

    @Test
    public void testSetHarga() {
        menus.setHarga(100);
        assertEquals(menus.getHarga(), 100);
    }
}
