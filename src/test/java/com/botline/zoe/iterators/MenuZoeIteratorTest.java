package com.botline.zoe.iterators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.botline.zoe.menus.MenuItem;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuZoeIteratorTest {

    private Class<?> menuZoeIteratorClass;
    ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
    MenuZoeIterator menuZoeIterator;
    int position = 0;

    @Before
    public void setUp() throws Exception {
        menuZoeIteratorClass = Class.forName(MenuZoeIterator.class.getName());
        MenuItem menuItem = new MenuItem("nasi ayam", 18000);
        menuItems.add(menuItem);
        this.menuZoeIterator = new MenuZoeIterator(menuItems);
    }

    @Test
    public void testNextMethod() {
        MenuItem menuItem = menuItems.get(position);
        position += 1;
        String namaMenuItemnya = menuItem.getNama();
        assertEquals("nasi ayam", namaMenuItemnya);
    }

    @Test
    public void testHasNextReturnTrue() {
        boolean hasNextTrue = menuZoeIterator.hasNext();
        assertTrue(hasNextTrue);
    }

    @Test
    public void testHasNextReturnFalse() {
        position = 5;
        menuItems.set(0, null);
        boolean hasNextFalse = menuZoeIterator.hasNext();
        assertFalse(hasNextFalse);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testHasNextThrowsException() {
        menuItems.remove(0);
        menuZoeIterator.next();
        menuZoeIterator.hasNext();
    }

    @Test
    public void testEveryLineInNextMethod() {
        menuZoeIterator.next();
        menuZoeIterator.hasNext();
    }
}