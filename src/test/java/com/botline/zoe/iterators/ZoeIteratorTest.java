package com.botline.zoe.iterators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZoeIteratorTest {

    private Class<?> iteratorClass;

    @Before
    public void setUp() throws Exception {
        iteratorClass = Class.forName(ZoeIterator.class.getName());
    }

    @Test
    public void testTypeIsInterface() {
        int classModifiers = iteratorClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testHasnextMethodReturnsBoolean() throws Exception {
        Method hasnext = iteratorClass.getDeclaredMethod("hasNext");
        Type retType = hasnext.getGenericReturnType();
        assertEquals("boolean", retType.getTypeName());
    }

    @Test
    public void testHasnextMethodIsAbstract() throws Exception {
        Method hasnext = iteratorClass.getDeclaredMethod("hasNext");
        int methodModifiers = hasnext.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testnextMethodIsAbstract() throws Exception {
        Method hasnext = iteratorClass.getDeclaredMethod("next");
        int methodModifiers = hasnext.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}
