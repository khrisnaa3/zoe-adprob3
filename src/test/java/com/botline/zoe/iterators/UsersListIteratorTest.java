package com.botline.zoe.iterators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Pembeli;
import com.botline.zoe.users.Users;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersListIteratorTest {

    private Class<?> usersListIteratorClass;
    ArrayList<Users> usersList = new ArrayList<Users>();
    UsersListIterator usersListIterator;
    int position = 0;

    @Before
    public void setUp() throws Exception {
        usersListIteratorClass = Class.forName(UsersListIterator.class.getName());
        Users users = new Counter("Ayam Tungtung", "ausdf98os");
        Users users1 = new Pembeli("Jamal", "oaisdjf984fo");
        usersList.add(users);
        usersList.add(users1);
        this.usersListIterator = new UsersListIterator(usersList);
    }

    @Test
    public void testNextMethod() {
        Users users = usersList.get(position);
        position += 1;
        String namaUser = users.getName();
        assertEquals("Ayam Tungtung", namaUser);
    }

    @Test
    public void testNextMethod2() {
        position += 1;
        Users users = usersList.get(position);
        String namaUser2 = users.getName();
        assertEquals("Jamal", namaUser2);
    }

    @Test
    public void testHasNextReturnTrue() {
        boolean hasNextTrue = usersListIterator.hasNext();
        assertTrue(hasNextTrue);
    }

    @Test
    public void testHasNextReturnFalse() {
        usersList.set(0, null);
        usersList.set(1, null);
        boolean userListNull0 = usersList.get(0) == null;
        boolean userListNull1 = usersList.get(1) == null;
        boolean hasNextFalse = usersListIterator.hasNext();
        assertTrue(userListNull0);
        assertTrue(userListNull1);
        assertFalse(hasNextFalse);
    }

    @Test
    public void testHasNextReturnFalse2() {
        usersListIterator.position = 4;
        boolean hasNextFalse = usersListIterator.hasNext();
        System.out.println(hasNextFalse);
        assertFalse(hasNextFalse);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testHasNextThrowsException() {
        usersList.remove(0);
        usersList.remove(1);
        usersListIterator.next();
        usersListIterator.hasNext();
    }

    @Test
    public void testEveryLineInNextMethod() {
        usersListIterator.next();
        usersListIterator.hasNext();
    }
}
