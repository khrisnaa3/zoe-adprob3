package com.botline.zoe.receivers;

import com.botline.zoe.menus.MenuItem;
import com.botline.zoe.users.Counter;
import com.linecorp.bot.client.exception.BadRequestException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;
import sun.nio.ch.ThreadPool;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PesanReceiverTest {

    private PesanReceiver pesanReceiver;
    private Counter counter;
    String param[] = new String[5];
    private MenuItem menu;

    @Before
    public void setUp() {
        pesanReceiver = new PesanReceiver();
        counter = new Counter("baba", "diwj9d1j8sa0");
        menu = new MenuItem("my menu", 1000);
        counter.addMenu(menu);
        pesanReceiver.setCounter(counter);

        param[0] = "pesan";
        param[1] = "baba";
        param[2] = "1";
        param[3] = "2";
        param[4] = "nama pembeli";

        pesanReceiver.doCommand(param, "id pembeli");
    }

    @Test
    public void testSetCounter() {
        assertEquals(pesanReceiver.counter, counter);
    }
}
