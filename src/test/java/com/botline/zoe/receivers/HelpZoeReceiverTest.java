package com.botline.zoe.receivers;

import static org.junit.Assert.assertEquals;

import com.botline.zoe.commands.HelpZoeCommand;
import com.botline.zoe.invoker.ZoeInvoker;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelpZoeReceiverTest {

    private ZoeInvoker invoker;
    private HelpZoeCommand helpCommand;
    private HelpZoeReceiver helpReceiver;

    @Before
    public void setUp() throws ClassNotFoundException {
        invoker = new ZoeInvoker();
        helpReceiver = new HelpZoeReceiver(invoker);
        helpCommand = new HelpZoeCommand(helpReceiver);

        invoker.setCommand(helpCommand);
    }

    @Test
    public void testInitialMessage() {
        String message = helpReceiver.getMessage();
        String target = "Hi there!\n These are the commands that you can tell me to do!\n";
        assertEquals(target, message);
    }

    @Test
    public void testDoCommand() {
        helpReceiver.doCommand();
        String message = helpReceiver.getMessage();
        String target = "Hi there!\n These are the commands that you can tell me to do!\n\n"
                + "1. "
                + helpCommand.toString()
                + "\n\n";

        assertEquals(message, target);
    }
}
