package com.botline.zoe.receivers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZoeReceiverTest {

    private ZoeReceiver receiver;
    private Class<?> receiverClass;
    private ZoeReceiver concreteReceiver;

    @Before
    public void setUp() throws Exception {
        receiverClass = Class.forName(ZoeReceiver.class.getName());
        receiver = mock(ZoeReceiver.class, Mockito.RETURNS_DEEP_STUBS);

        when(receiver.getMessage()).thenCallRealMethod();
        Mockito.doCallRealMethod().when(receiver).setMessage(anyString());

        concreteReceiver = new ZoeReceiver() {
            @Override
            public void doCommand() {
                setMessage("aaa");
            }
        };
    }

    @Test
    public void testTypeIsAbstract() {
        int classModifiers = receiverClass.getModifiers();
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testSetGetMessage() {
        receiver.setMessage("baru lagi");
        assertEquals(receiver.getMessage(), "baru lagi");
    }

    @Test
    public void testGetMethodReturnsString() throws Exception {
        Method getMessage = receiverClass.getDeclaredMethod("getMessage");
        Type retType = getMessage.getGenericReturnType();
        assertEquals("java.lang.String", retType.getTypeName());
    }

    @Test
    public void testDoCommandMethodIsAbstract() throws Exception {
        Method execute = receiverClass.getDeclaredMethod("doCommand");
        int methodModifiers = execute.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testSetGetConcreteReceiver() {
        concreteReceiver.doCommand();
        String message = concreteReceiver.getMessage();
        assertEquals(message, "aaa");
    }
}
