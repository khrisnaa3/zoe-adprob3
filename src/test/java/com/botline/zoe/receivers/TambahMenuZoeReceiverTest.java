package com.botline.zoe.receivers;

import static org.junit.Assert.assertEquals;

import com.botline.zoe.menus.MenuItem;
import com.botline.zoe.users.Counter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TambahMenuZoeReceiverTest {

    private TambahMenuZoeReceiver tambahMenuZoeReceiver;
    private Counter counter;
    String[] param = new String[4];
    MenuItem menu;

    @Before
    public void setUp() {
        tambahMenuZoeReceiver = new TambahMenuZoeReceiver();
        counter = new Counter("nama counter", "ifoja8dfu9");
        tambahMenuZoeReceiver.setCounter(counter);
        param[0] = "Tambah_Menu";
        param[1] = "nama counter";
        param[2] = "nama menu";
        param[3] = "10000";
        tambahMenuZoeReceiver.doCommand(param, "ifoja8dfu9");
        menu = tambahMenuZoeReceiver.menu;
    }

    @Test
    public void testSetCounter() {
        assertEquals(tambahMenuZoeReceiver.counter, counter);
    }

    @Test
    public void testMessageIsCorrect() {
        String message = "Counter nama counter"
                + " has added a new menu "
                + "nama menu"
                + " with a price of "
                + "10000" + ".\n"
                + "Very delicious!!";
        String curMessage = tambahMenuZoeReceiver.getMessage();
        assertEquals(curMessage, message);
    }

    @Test
    public void testIncorrectCounter() {
        String message = "You are not the owner of this counter!"
                + "\n Sorry :(";
        param[2] = "fake menu name";
        tambahMenuZoeReceiver.doCommand(param, "wrongId");
        String curMessage = tambahMenuZoeReceiver.getMessage();
        assertEquals(curMessage, message);
    }

    @Test
    public void testCounterNotFound() {
        tambahMenuZoeReceiver.setCounter(null);
        param[2] = "fake menu name";
        tambahMenuZoeReceiver.doCommand(param, "fakeId");
        String message = "Counter not found! :( Make sure you already "
                + "register and type the right counter name";
        String curMessage = tambahMenuZoeReceiver.getMessage();
        assertEquals(curMessage, message);
    }

    @Test
    public void testPriceIsNotNumber() {
        param[2] = "fake menu name";
        param[3] = "harga";
        tambahMenuZoeReceiver.doCommand(param, "ifoja8dfu9");
        String message = "Please input the price as number!";
        String curMessage = tambahMenuZoeReceiver.getMessage();
        assertEquals(curMessage, message);
    }
}
