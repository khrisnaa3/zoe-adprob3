package com.botline.zoe.receivers.listwaitress;

import static org.junit.Assert.assertEquals;

import com.botline.zoe.receivers.userslists.PembeliList;
import com.botline.zoe.users.Pembeli;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ListPembeliWaitressTest {

    private Pembeli pembeli;
    private PembeliList pembeliList;
    private ListPembeliWaitress listPembeliWaitress;

    @Before
    public void setUp() throws Exception {
        pembeli = new Pembeli("nama", "asdoifja0");
        pembeliList = new PembeliList();
        pembeliList.listOfUsers.add(pembeli);
        listPembeliWaitress = new ListPembeliWaitress(pembeliList);
    }

    @Test
    public void testDoCommand() {
        listPembeliWaitress.doCommand();
        String message = listPembeliWaitress.getMessage();
        String target = "List of Pembeli\n"
                + "1. "
                + pembeli.getName()
                + "\n";

        assertEquals(message, target);
    }
}
