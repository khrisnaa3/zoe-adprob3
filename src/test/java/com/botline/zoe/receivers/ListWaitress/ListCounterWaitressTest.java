package com.botline.zoe.receivers.listwaitress;

import static org.junit.Assert.assertEquals;

import com.botline.zoe.receivers.userslists.CounterList;
import com.botline.zoe.users.Counter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ListCounterWaitressTest {

    private Counter counter;
    private CounterList counterList;
    private ListCounterWaitress listCounterWaitress;

    @Before
    public void setUp() throws Exception {
        counter = new Counter("nama", "asdoifja0");
        counterList = new CounterList();
        counterList.listOfUsers.add(counter);
        listCounterWaitress = new ListCounterWaitress(counterList);
    }

    @Test
    public void testDoCommand() {
        listCounterWaitress.doCommand();
        String message = listCounterWaitress.getMessage();
        String target = "List of Counters\n"
                + "1. "
                + counter.getName()
                + "\n";

        assertEquals(message, target);
    }
}
