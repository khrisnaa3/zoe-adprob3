package com.botline.zoe.receivers;

import static org.junit.Assert.assertEquals;

import com.botline.zoe.menus.MenuItem;
import com.botline.zoe.receivers.userslists.CounterList;
import com.botline.zoe.receivers.userslists.UsersList;
import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Users;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuWaitressTest {

    private UsersList usersList;
    private Counter user1;
    private Counter user2;
    private MenuWaitress menuWaitress;
    private MenuItem menu1;

    @Before
    public void setUp() {
        usersList = new CounterList();
        user1 = new Counter("tes1", "adsuhf9");
        user2 = new Counter("tes2", "ija9dfj0");
        menu1 = new MenuItem("Ayam", 1000);
        usersList.setNewUser(user1);
        usersList.doCommand();
        usersList.setNewUser(user2);
        usersList.doCommand();
        user1.addMenu(menu1);
        user2.addMenu(menu1);
        menuWaitress = new MenuWaitress(usersList);
    }

    @Test
    public void testLengthOfList() {
        ArrayList<Users> theList = menuWaitress.listOfUsers;
        assertEquals(theList.size(), 2);
    }

    @Test
    public void testDoCommand() {
        menuWaitress.doCommand();
        String trueReply = menuWaitress.getMessage();
        String expectedReply = "Menu for tes1 counter:\n"
                + "1. Ayam @1000\n\n"
                + "Menu for tes2 counter:\n"
                + "1. Ayam @1000\n\n";
        assertEquals(trueReply, expectedReply);
    }
}
