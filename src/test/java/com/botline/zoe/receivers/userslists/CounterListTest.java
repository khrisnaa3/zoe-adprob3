package com.botline.zoe.receivers.userslists;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.botline.zoe.iterators.UsersListIterator;
import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Users;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CounterListTest {

    @Spy
    private CounterList counterList;

    private Users user;

    @Before
    public void setUp() throws ClassNotFoundException {
        user = new Counter("tes", "asoidfj8");
        String[] arr = new String[3];
        arr[1] = "tes aja";
        counterList.doCommand(arr, "oaisjdf09");
        user = counterList.newUser;
    }

    @Test
    public void testDoCommand() {
        assertEquals(user.getName(), "tes aja");

        ArrayList<Users> theList = counterList.listOfUsers;
        assertEquals(theList.size(), 1);

        assertTrue(counterList.newUser instanceof Counter);
    }

    @Test
    public void testMessageNewCounter() {
        String message = counterList.getMessage();
        String target = "Counter " + "tes aja"
                + " has been successfully created.\n"
                + "Zoe Welcomes You!\n";

        assertEquals(message, target);
    }

    @Test
    public void testMessageEqualNameCounter() {
        String[] arr = new String[3];
        arr[1] = "tes aja";
        counterList.doCommand(arr, "another Id");
        String message = counterList.getMessage();
        String target = "Counter " + "tes aja"
                + " has already been listed!\n"
                + "Please try another name! :)\n";

        assertEquals(message, target);
    }

    @Test
    public void testMessageEqualIdCounter() {
        String[] arr = new String[3];
        arr[1] = "other name";
        counterList.doCommand(arr, "oaisjdf09");
        String message = counterList.getMessage();
        String target = "You have already registered"
                + " as a counter!\n"
                + "Zoe can't have 2 of you right?\n";

        assertEquals(message, target);
    }

    @Test
    public void testCreateIterator() {
        UsersListIterator counterListIterator = (UsersListIterator) counterList.createIterator();
        ArrayList<Users> iteratorArrayList = counterListIterator.getList();
        ArrayList<Users> counterArrayList = counterList.listOfUsers;
        assertEquals(iteratorArrayList, counterArrayList);
    }
}
