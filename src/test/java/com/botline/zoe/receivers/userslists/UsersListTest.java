package com.botline.zoe.receivers.userslists;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import com.botline.zoe.users.Pembeli;
import com.botline.zoe.users.Users;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersListTest {

    @Spy
    private UsersList usersList;

    private Users user;
    private Users user2;

    @Before
    public void setUp() throws ClassNotFoundException {
        user = new Pembeli("tes", "a0sjdf09a");
        user2 = new Pembeli("tes2", "a0isdf09");
    }

    @Test
    public void testAddUser() {
        usersList.addUsers(user);
        ArrayList<Users> theList = usersList.listOfUsers;
        assertEquals(theList.size(), 1);
    }

    @Test
    public void testDoCommand() {
        usersList.newUser = user;
        usersList.doCommand();
        ArrayList<Users> theList = usersList.listOfUsers;
        assertEquals(theList.size(), 1);
    }

    @Test
    public void testUserExistNameAndId() {
        usersList.addUsers(user);
        usersList.addUsers(user2);
        Users found = usersList.findUser("tes2", "a0isdf09");
        assertEquals(found, user2);
    }

    @Test
    public void testUserExistName() {
        usersList.addUsers(user);
        usersList.addUsers(user2);
        Users found = usersList.findUser("tes2", "fake Id");
        assertEquals(found, user2);
    }

    @Test
    public void testUserExistId() {
        usersList.addUsers(user);
        usersList.addUsers(user2);
        Users found = usersList.findUser("fake name", "a0isdf09");
        assertEquals(found, user2);
    }

    @Test
    public void testFindUserNotExist() {
        usersList.addUsers(user);
        usersList.addUsers(user2);
        Users found = usersList.findUser("tes lala", "fake Id");
        assertNull(found);
    }

    @Test
    public void testSetNewUser() {
        usersList.setNewUser(user);
        assertEquals(user, usersList.getNewUser());
    }
}
