package com.botline.zoe.receivers.userslists;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.botline.zoe.iterators.UsersListIterator;
import com.botline.zoe.users.Pembeli;
import com.botline.zoe.users.Users;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PembeliListTest {

    @Spy
    private PembeliList pembeliList;

    private Users user;

    @Before
    public void setUp() throws ClassNotFoundException {
        user = new Pembeli("tes", "09adjf09");
        pembeliList = new PembeliList();
        String[] arr = new String[3];
        arr[1] = "tes aja";
        pembeliList.doCommand(arr, "09adjf09");
        user = pembeliList.newUser;
    }

    @Test
    public void testDoCommand() {
        assertEquals(user.getName(), "tes aja");

        ArrayList<Users> theList = pembeliList.listOfUsers;
        assertEquals(theList.size(), 1);

        assertTrue(pembeliList.newUser instanceof Pembeli);
    }

    @Test
    public void testCorrectId() {
        String userId = user.getUserId();
        assertEquals(userId, "09adjf09");
    }

    @Test
    public void testMessageNewPembeli() {
        String message = pembeliList.getMessage();
        String target = "Pembeli " + "tes aja"
                + " has been successfully created.\n"
                + "Welcome! Please take a look at our menu.\n";

        assertEquals(message, target);
    }

    @Test
    public void testMessageEqualNamePembeli() {
        String[] arr = new String[3];
        arr[1] = "tes aja";
        pembeliList.doCommand(arr, "another Id");
        String message = pembeliList.getMessage();
        String target = "Pembeli " + "tes aja"
                + " already exist.\n"
                + "Please try another unique name! :)\n";
        assertEquals(message, target);
    }

    @Test
    public void testMessageEqualIdPembeli() {
        String[] arr = new String[3];
        arr[1] = "other name";
        pembeliList.doCommand(arr, "09adjf09");
        String message = pembeliList.getMessage();
        String target = "You have already "
                + "registered as a buyer!\n"
                + "You can't be two buyers"
                + " at once right?\n";

        assertEquals(message, target);
    }

    @Test
    public void testCreateIterator() {
        UsersListIterator pembeliListIterator = (UsersListIterator) pembeliList.createIterator();
        ArrayList<Users> iteratorArrayList = pembeliListIterator.getList();
        ArrayList<Users> pembeliArrayList = pembeliList.listOfUsers;
        assertEquals(iteratorArrayList, pembeliArrayList);
    }
}
