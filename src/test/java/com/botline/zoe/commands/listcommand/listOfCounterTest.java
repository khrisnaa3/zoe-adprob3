package com.botline.zoe.commands.listcommand;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.receivers.listwaitress.ListCounterWaitress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class listOfCounterTest {

    private Class<?> listOfCounterClass;
    private ListCounterWaitress listCounterWaitress;
    private ListOfCounterCommand listOfCounterCommand;

    @Before
    public void setUp() throws Exception {
        listOfCounterClass = Class.forName(ListOfCounterCommand.class.getName());
        listCounterWaitress = mock(ListCounterWaitress.class);
        listOfCounterCommand = new ListOfCounterCommand(listCounterWaitress);
    }

    @Test
    public void testListOfCounterZoeCommand() {
        boolean isCommand = listOfCounterCommand instanceof ZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testToString() {
        String toStr = listOfCounterCommand.toString();
        String actualString = "-- ListOfCounterCommand --\n"
                + "Use this command by typing \"List_counter\".\n"
                + "This command show you registered counters in Zoe.\n";
        assertEquals(toStr, actualString);
    }

    @Test
    public void testExecuteMethodIsCalled() {
        String[] arr = new String[1];
        arr[0] = "listOfCounter";
        listOfCounterCommand.execute(arr, "widf78ey7");
        verify(listCounterWaitress).doCommand();
    }
}
