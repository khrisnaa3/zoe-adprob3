package com.botline.zoe.commands.listcommand;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.receivers.listwaitress.ListPembeliWaitress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class listOfPembeliTest {

    private Class<?> listOfPembeliClass;
    private ListPembeliWaitress listPembeliWaitress;
    private ListOfPembeliCommand listOfPembeliCommand;

    @Before
    public void setUp() throws Exception {
        listOfPembeliClass = Class.forName(ListOfPembeliCommand.class.getName());
        listPembeliWaitress = mock(ListPembeliWaitress.class);
        listOfPembeliCommand = new ListOfPembeliCommand(listPembeliWaitress);
    }

    @Test
    public void testListOfCounterZoeCommand() {
        boolean isCommand = listOfPembeliCommand instanceof ZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testToString() {
        String toStr = listOfPembeliCommand.toString();
        String actualString = "-- ListOfPembeliCommand --\n"
                + "Use this command by typing \"List_pembeli\".\n"
                + "This command show you registered buyers in Zoe.\n";
        assertEquals(toStr, actualString);
    }

    @Test
    public void testExecuteMethodIsCalled() {
        String[] arr = new String[1];
        arr[0] = "listOfPembeli";
        listOfPembeliCommand.execute(arr, "aosidjf0");
        verify(listPembeliWaitress).doCommand();
    }
}
