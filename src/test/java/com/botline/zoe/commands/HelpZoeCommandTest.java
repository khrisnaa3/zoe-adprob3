package com.botline.zoe.commands;

import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.invoker.ZoeInvoker;
import com.botline.zoe.receivers.HelpZoeReceiver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelpZoeCommandTest {

    private HelpZoeCommand helpCommand;
    private ZoeInvoker zoeInvoker;
    private HelpZoeReceiver helpZoeReceiver;

    @Before
    public void setUp() {
        zoeInvoker = mock(ZoeInvoker.class);
        helpZoeReceiver = mock(HelpZoeReceiver.class);
        helpCommand = new HelpZoeCommand(helpZoeReceiver);
    }

    @Test
    public void testHelpCommandIsCommand() {
        boolean isCommand = helpCommand instanceof ZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testExecuteMethodIsCalled() {
        String[] arr = new String[1];
        arr[0] = "help";
        helpCommand.execute(arr, "aosidf09");
        verify(helpZoeReceiver).doCommand();
    }
}
