package com.botline.zoe.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ZoeCommandTest {

    private Class<?> commandClass;

    @Before
    public void setUp() throws Exception {
        commandClass = Class.forName(ZoeCommand.class.getName());
    }

    @Test
    public void testTypeIsInterface() {
        int classModifiers = commandClass.getModifiers();
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testExecuteMethodReturnsVoid() throws Exception {
        Method execute = commandClass.getDeclaredMethod("execute", String[].class, String.class);
        Type retType = execute.getGenericReturnType();
        assertEquals("void", retType.getTypeName());
    }

    @Test
    public void testExecuteMethodIsAbstract() throws Exception {
        Method execute = commandClass.getDeclaredMethod("execute", String[].class, String.class);
        int methodModifiers = execute.getModifiers();
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}

