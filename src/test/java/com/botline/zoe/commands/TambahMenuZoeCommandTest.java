package com.botline.zoe.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.receivers.TambahMenuZoeReceiver;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TambahMenuZoeCommandTest {

    private TambahMenuZoeReceiver tambahMenuZoeReceiver;
    private Class<?> tambahmenuclass;
    private TambahMenuZoeCommand tambahmenu;

    @Before
    public void setUp() throws Exception {
        tambahmenuclass = Class.forName(TambahMenuZoeCommand.class.getName());
        tambahMenuZoeReceiver = mock(TambahMenuZoeReceiver.class);
        tambahmenu = new TambahMenuZoeCommand(tambahMenuZoeReceiver);
    }

    @Test
    public void testTambahMenuCommandIsCommand() {
        boolean isCommand = tambahmenu instanceof ZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testtoString() {
        String str = "-- Zoe's TambahMenu Command --\n"
                + "Use this command by typing \"Tambah_Menu [nama_counter] "
                + "[nama_menu] [harga_menu]\".\n"
                + "This command will apply if user authority is counter.";
        assertEquals(str, tambahmenu.toString());
    }

    @Test
    public void testExecuteMethodIsCalled() {
        String[] str = {"Tambah_Menu", "namacounter", "menu", "1000"};
        tambahmenu.execute(str, "aisdjf9aj");
        verify(tambahMenuZoeReceiver).doCommand(str, "aisdjf9aj");
    }
}



