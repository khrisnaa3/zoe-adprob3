package com.botline.zoe.commands;

import com.botline.zoe.receivers.PesanReceiver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class PesanCommandTest {
    private PesanCommand pesanCommand;
    private PesanReceiver pesanReceiver;


    @Before
    public void setUp() {
        pesanReceiver = mock(PesanReceiver.class);
        pesanCommand = new PesanCommand(pesanReceiver);
    }

    @Test
    public void testPesanCommandIsCommand() {
        boolean isCommand = pesanCommand instanceof ZoeCommand;
        assertTrue(isCommand);
    }


    @Test
    public void testExecuteMethodIsCalled() {
        String[] arr = new String[4];
        arr[0] = "pesan";
        arr[1] = "baba";
        arr[2] = "3";
        arr[3] = "5";
        pesanCommand.execute(arr, "sdif0s8uv8c");
        verify(pesanReceiver).doCommand(arr, "sdif0s8uv8c");
    }

    @Test
    public void testToString() {
        String str = "-- Zoe's Order Command --\n"
                + "Use this command by typing \"PESAN NAMA_COUNTER NOMOR_MENU JUMLAH\".\n"
                + "Example : pesan counterAyam 3 2";

        assertEquals(str, pesanCommand.toString());
    }
}
