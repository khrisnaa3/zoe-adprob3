package com.botline.zoe.commands.daftarcommand;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.receivers.userslists.UsersList;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaftarZoeCommandTest {

    private Class<?> daftarCommandClass;
    private UsersList usersList;
    private DaftarZoeCommand daftarZoeCommand;

    @Before
    public void setUp() throws Exception {
        daftarCommandClass = Class.forName(DaftarZoeCommand.class.getName());
        usersList = mock(UsersList.class);
        daftarZoeCommand = new DaftarZoeCommand(usersList) {
            @Override
            public String toString() {
                return null;
            }
        };
    }

    @Test
    public void testHelpCommandIsCommand() {
        boolean isCommand = daftarZoeCommand instanceof ZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testExecuteMethodIsCalled() {
        String[] arr = new String[2];
        arr[0] = "daftar";
        arr[1] = "user";
        daftarZoeCommand.execute(arr, "asoidjf0208");
        verify(usersList).doCommand(arr, "asoidjf0208");
    }

    @Test
    public void testToStringReturnString() throws Exception {
        Method toString = daftarCommandClass.getDeclaredMethod("toString");
        Type retType = toString.getGenericReturnType();
        assertEquals("java.lang.String", retType.getTypeName());
    }
}
