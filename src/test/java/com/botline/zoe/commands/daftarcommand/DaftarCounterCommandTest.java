package com.botline.zoe.commands.daftarcommand;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import com.botline.zoe.receivers.userslists.UsersList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaftarCounterCommandTest {
    private Class<?> daftarCounterClass;
    private UsersList usersList;
    private DaftarCounterCommand daftarCounterCommand;

    @Before
    public void setUp() throws Exception {
        daftarCounterClass = Class.forName(DaftarCounterCommand.class.getName());
        usersList = mock(UsersList.class);
        daftarCounterCommand = new DaftarCounterCommand(usersList);
    }

    @Test
    public void testDaftarCounterCommandIsDaftarZoeCommand() {
        boolean isCommand = daftarCounterCommand instanceof DaftarZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testToString() {
        String toStr = daftarCounterCommand.toString();
        assertEquals(toStr, "-- Zoe's Register Counter Command --\n"
                + "Use this command by typing \"DAFTAR [nama_user] Counter\".\n"
                + "This command will register you as our counter.");
    }
}
