package com.botline.zoe.commands.daftarcommand;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import com.botline.zoe.receivers.userslists.UsersList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaftarPembeliCommandTest {

    private Class<?> daftarPembeliClass;
    private UsersList usersList;
    private DaftarPembeliCommand daftarPembeliCommand;

    @Before
    public void setUp() throws Exception {
        daftarPembeliClass = Class.forName(DaftarPembeliCommand.class.getName());
        usersList = mock(UsersList.class);
        daftarPembeliCommand = new DaftarPembeliCommand(usersList);
    }

    @Test
    public void testDaftarPembeliCommandIsDaftarZoeCommand() {
        boolean isCommand = daftarPembeliCommand instanceof DaftarZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testToString() {
        String toStr = daftarPembeliCommand.toString();
        assertEquals(toStr, "-- Zoe's Register Buyer Command --\n"
                + "Use this command by typing \"DAFTAR [nama_user] Pembeli\".\n"
                + "This command will register you as our Buyer.");
    }
}
