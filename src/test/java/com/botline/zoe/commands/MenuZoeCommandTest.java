package com.botline.zoe.commands;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.receivers.MenuWaitress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuZoeCommandTest {

    private MenuZoeCommand menuZoeCommand;
    private MenuWaitress menuWaitress;

    @Before
    public void setUp() {
        menuWaitress = mock(MenuWaitress.class);
        menuZoeCommand = new MenuZoeCommand(menuWaitress);
    }

    @Test
    public void testHelpCommandIsCommand() {
        boolean isCommand = menuZoeCommand instanceof ZoeCommand;
        assertTrue(isCommand);
    }

    @Test
    public void testExecuteMethodIsCalled() {
        String[] arr = new String[1];
        arr[0] = "menu";
        menuZoeCommand.execute(arr, "asud7989e  ");
        verify(menuWaitress).doCommand();
    }

    @Test
    public void testToString() {
        String str = "-- Zoe's Menu Command --\n"
                + "Use this command by typing \"MENU\".\n";
        assertEquals(str, menuZoeCommand.toString());
    }
}
