package com.botline.zoe.invoker;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.commands.ZoeCommand;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZoeInvokerTests {
    private Class<?> invokerClass;
    private ZoeInvoker zoeInvoker;
    private ZoeCommand zoeCommand;
    private ArrayList<ZoeCommand> commandsList;

    @Before
    public void setUp() throws Exception {
        invokerClass = Class.forName(ZoeInvoker.class.getName());
        zoeInvoker = new ZoeInvoker();
        zoeCommand = mock(ZoeCommand.class);
        commandsList = zoeInvoker.commandsList;
        zoeInvoker.setCommand(zoeCommand);
    }

    @Test
    public void testSetCommand() {
        assertEquals(1, commandsList.size());
    }

    @Test
    public void testSetCommandIdx() {
        zoeInvoker.setCommand(0,zoeCommand);
        ArrayList<ZoeCommand> zoeCommands = zoeInvoker.getCommandsList();
        ZoeCommand zoeCommand = zoeCommands.get(0);
        assertEquals(zoeCommand, this.zoeCommand);
    }

    @Test
    public void testButtonPressedExecutesCommand() {
        String[] arr = new String[1];
        arr[0] = "help";
        zoeInvoker.buttonPressed(0, arr, "aidj9dus");
        verify(zoeCommand).execute(arr, "aidj9dus");
    }

    @Test
    public void testButtonPressedMethodReturnsVoid() throws Exception {
        Method buttonPressed = invokerClass.getDeclaredMethod("buttonPressed",
                int.class, String[].class, String.class);
        Type returnType = buttonPressed.getGenericReturnType();
        assertEquals("void", returnType.getTypeName());
    }
}
