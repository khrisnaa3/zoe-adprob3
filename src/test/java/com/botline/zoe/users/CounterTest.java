package com.botline.zoe.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.botline.zoe.menus.MenuItem;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@RunWith(SpringRunner.class)
@SpringBootTest
public class CounterTest {

    private Counter counter;

    @Before
    public void setUp() {
        counter = new Counter("Test Counter", "e18ueuo");
    }

    @Test
    public void testIsCounter() {
        assertFalse(counter.isPembeli());
    }

    @Test
    public void testAddMenuToList() {
        MenuItem newMenu = new MenuItem("Nasi Goreng", 12000);
        ArrayList<MenuItem> menuItems = counter.menuItems;
        counter.addMenu(newMenu);
        assertEquals(menuItems.size(), 1);
    }

    @Test
    public void testGetMenu() {
        ArrayList<MenuItem> menuItems = counter.getmenu();
        assertTrue(menuItems instanceof ArrayList);
    }
}