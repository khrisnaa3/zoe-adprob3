package com.botline.zoe.users;

import static org.junit.Assert.assertEquals;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsersTest {

    private Users user;

    @Before
    public void setUp() throws Exception {
        user = mock(Users.class, Mockito.RETURNS_DEEP_STUBS);

        when(user.getName()).thenCallRealMethod();
        Mockito.doCallRealMethod().when(user).setName(anyString());

        when(user.isPembeli()).thenCallRealMethod();
        Mockito.doCallRealMethod().when(user).setPembeli(anyBoolean());

        when(user.getUserId()).thenCallRealMethod();
        Mockito.doCallRealMethod().when(user).setUserId(anyString());
    }

    @Test
    public void testSetGetName() {
        user.setName("baru lagi");
        assertEquals(user.getName(), "baru lagi");
    }

    @Test
    public void testSetGetPembeli() {
        user.setPembeli(true);
        assertEquals(true, user.isPembeli());
    }

    @Test
    public void testSetGetId() {
        user.setUserId("aaa");
        String id = user.getUserId();
        assertEquals("aaa", id);
    }

}
