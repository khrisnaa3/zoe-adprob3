package com.botline.zoe.users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PembeliTest {

    private Pembeli pembeli;

    @Before
    public void setUp() {
        pembeli = new Pembeli("Test", "osa918ui");
    }

    @Test
    public void testIsPembeli() {
        assertTrue(pembeli.isPembeli());
    }

    @Test
    public void testPembeliIsUser() {
        boolean isUser = pembeli instanceof Users;
        assertTrue(isUser);
    }

}
