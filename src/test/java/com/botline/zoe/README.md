# Zoe - Advance Programming Project B3

## Project Test Code Home
This is the main test code of the Zoe Chatbot program. Each folder consists 
of tests of a single
app that may be further developed in the future.

## Project Developers

- Raihansyah Attallah Andrian (1706040196)
- Daniel Anderson Estefan (1706039641)
- Dimas Aditya Faiz (1706039686)
- Fanisya Dewi N. (1706039894)
- Stefanus Khrisna Aji Hardiyanto (1706074921)

## Claims

As developers, we hope that Zoe may satisfy both buyers and sellers needs.
We are very sorry if there are any inconvenience when using our chatbot Zoe.

If you wish to report any bugs on our bot that you find please contact
one of our developers directly.

## Project Task Checklist

- [x] Week 1: Create tests for Users, Pembeli, and Counter class
- [x] Week 1: Create tests for Invoker, Menus, Commands


## My Notes - Project Test Code Home

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1.