package com.botline.zoe.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.invoker.ZoeInvoker;
import com.botline.zoe.receivers.HelpZoeReceiver;
import com.botline.zoe.receivers.ZoeReceiver;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ZoeControllerTest {

    private Class<?> zoeMain;
    private ZoeController zoeController;
    private ZoeInvoker zoeInvokerMocked;
    private ZoeReceiver helpZoeReceiver;

    @Before
    public void setUp() throws Exception {
        zoeMain = Class.forName(ZoeController.class.getName());
        zoeController = new ZoeController();
        zoeInvokerMocked = mock(ZoeInvoker.class);
        zoeController.setZoeInvoker(zoeInvokerMocked);
        helpZoeReceiver = new HelpZoeReceiver(zoeInvokerMocked);
        helpZoeReceiver.setMessage("tes message");
    }

    @Test
    public void testZoeMainHasCreateCommandMethod() throws Exception {
        Method createCommands = zoeMain.getMethod("createCommands");
        int methodModifiers = createCommands.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", createCommands.getGenericReturnType().getTypeName());
    }

    @Test
    public void testZoeMainHasSetCommandMethod() throws Exception {
        Method setCommands = zoeMain.getMethod("setCommands");
        int methodModifiers = setCommands.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", setCommands.getGenericReturnType().getTypeName());
    }

    @Test
    public void testZoeMainPressZoeInvokerMethod() throws Exception {
        Method pressZoeInvoker = zoeMain.getDeclaredMethod("pressZoeInvoker",
                int.class, String[].class, String.class);
        Collection<Parameter> parameters = Arrays.asList(pressZoeInvoker.getParameters());
        int methodModifiers = pressZoeInvoker.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testGetInvoker() {
        ZoeInvoker theInvoker = zoeController.getZoeInvoker();
        boolean check = theInvoker instanceof ZoeInvoker;
        assertTrue(check);
    }

    @Test
    public void testButtonPressedExecutesCommand() {
        String[] arr = new String[1];
        arr[0] = "help";
        zoeController.pressZoeInvoker(0, arr, "sdij9ds8");
        verify(zoeInvokerMocked).buttonPressed(0, arr, "sdij9ds8");
    }

    @Test
    public void testGetZoeCommands() {
        ArrayList<ZoeCommand> zoeCommands = zoeController.getZoeCommands();
        assertTrue(zoeCommands instanceof ArrayList);
    }

    @Test
    public void testGetZoeReceivers() {
        ArrayList<ZoeReceiver> zoeReceivers = zoeController.getZoeReceivers();
        assertTrue(zoeReceivers instanceof ArrayList);
    }

    @Test
    public void testSetZoeReceivers() {
        ArrayList<ZoeReceiver> zoeReceivers = new ArrayList<>();
        zoeReceivers.add(helpZoeReceiver);
        zoeController.setZoeReceivers(zoeReceivers);
        ArrayList<ZoeReceiver> realReceivers = zoeController.getZoeReceivers();
        assertEquals(realReceivers, zoeReceivers);
    }

    @Test
    public void testGetReply() {
        String reply = zoeController.getReply(helpZoeReceiver);
        assertEquals(reply, "tes message");
    }

    @Test
    public void testTambahMenuPrepare() {

    }

    @Test
    public void mapIsSet() {
        zoeController.setMap();
        int map = zoeController.getMap().get("help");
        int map2 = zoeController.getMap().get("daftar");
        int map3 = zoeController.getMap().get("tambah_menu");
        assertEquals(map, 1);
        assertEquals(map2, 3);
        assertEquals(map3, 4);
    }

    @Test
    public void testValidLength() {
        int map = zoeController.getMapValidlength("help");
        assertEquals(map, 1);
    }




}
