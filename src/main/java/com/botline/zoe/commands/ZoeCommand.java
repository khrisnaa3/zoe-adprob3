package com.botline.zoe.commands;

public interface ZoeCommand {

    void execute(String[] parameters, String userId);

    @Override
    public String toString();
}
