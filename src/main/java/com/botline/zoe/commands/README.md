# Zoe - Advance Programming Project B3

## Commands - App Description

This app consists of classes that represents Zoe's Commands. Each Command
will have its own concrete class. Each concrete zoeCommand class implements
the Command interface.

## Commands - App Contents

### Classes

1. Command - The Interface
2. HelpCommand - Concrete zoeCommand to the zoeCommand "Help"
3. DaftarCommand - Concrete Command that can enlist a new user as 
Pembeli or Counter.

### Utilities

1. Represents each zoeCommand that Zoe is able to create.
2. The main classes that defines the Command Pattern used in Zoe
3. May be called by other classes to represent the commands


## App Task Checklist

### Week 1
- [x] Week 1: Create Command Interface in Commands app
- [x] Week 1: Create HelpCommand Class in Commands app
- [x] Week 1: Create tests for Command classes

### Week 2
- [ ] Week 2: Test for the DaftarCommand Class [Stefanus Khrisna]
- [ ] Week 2: Implement the DaftarCommand Class [Stefanus Khrisna]

Create the DaftarCommand class. It should hold the receiver of this
command (the UsersList class as the receiver).

## My Notes - Users App

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1. Help Command: Receiver adalah Invoker (Karena punya list of zoeCommand)