package com.botline.zoe.commands.listcommand;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.receivers.listwaitress.ListCounterWaitress;

public class ListOfCounterCommand implements ZoeCommand {

    ListCounterWaitress listCounterWaitress;

    public ListOfCounterCommand(ListCounterWaitress listCounterWaitress) {
        this.listCounterWaitress = listCounterWaitress;
    }

    @Override
    public void execute(String[] parameters, String userId) {
        this.listCounterWaitress.doCommand();
    }

    @Override
    public String toString() {
        String str = "-- ListOfCounterCommand --"
                + "\nUse this command by typing \"List_counter\".\n"
                + "This command show you registered counters in Zoe.\n";
        return str;
    }
}
