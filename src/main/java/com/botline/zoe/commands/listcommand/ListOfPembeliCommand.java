package com.botline.zoe.commands.listcommand;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.receivers.listwaitress.ListPembeliWaitress;

public class ListOfPembeliCommand implements ZoeCommand {

    ListPembeliWaitress listPembeliWaitress;

    public ListOfPembeliCommand(ListPembeliWaitress listPembeliWaitress) {
        this.listPembeliWaitress = listPembeliWaitress;
    }

    @Override
    public void execute(String[] parameters, String userId) {
        this.listPembeliWaitress.doCommand();
    }

    @Override
    public String toString() {
        String str = "-- ListOfPembeliCommand --"
                + "\nUse this command by typing \"List_pembeli\".\n"
                + "This command show you registered buyers in Zoe.\n";;
        return str;
    }
}

