package com.botline.zoe.commands;

import com.botline.zoe.receivers.MenuWaitress;

public class MenuZoeCommand implements ZoeCommand {

    MenuWaitress menuWaitress;

    public MenuZoeCommand(MenuWaitress menuWaitress) {
        this.menuWaitress = menuWaitress;
    }

    @Override
    public void execute(String[] parameters, String userId) {
        this.menuWaitress.doCommand();
    }

    @Override
    public String toString() {
        String str = "-- Zoe's Menu Command --\n"
                + "Use this command by typing \"MENU\".\n";

        return str;
    }
}
