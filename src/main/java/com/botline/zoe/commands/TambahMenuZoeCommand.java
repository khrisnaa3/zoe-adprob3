package com.botline.zoe.commands;

import com.botline.zoe.receivers.TambahMenuZoeReceiver;

public class TambahMenuZoeCommand implements ZoeCommand {

    TambahMenuZoeReceiver tambahMenuZoeReceiver;

    public TambahMenuZoeCommand(TambahMenuZoeReceiver tambahMenuZoeReceiver) {
        this.tambahMenuZoeReceiver = tambahMenuZoeReceiver;
    }

    @Override
    public void execute(String[] parameters, String userId) {
        this.tambahMenuZoeReceiver.doCommand(parameters, userId);
    }

    @Override
    public String toString() {
        String str = "-- Zoe's TambahMenu Command --\n"
                + "Use this command by typing \"Tambah_Menu [nama_counter] [nama_menu]"
                + " [harga_menu]\".\n"
                + "This command will apply if user authority is counter.";

        return str;
    }
}
