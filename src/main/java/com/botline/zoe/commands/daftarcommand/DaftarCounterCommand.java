package com.botline.zoe.commands.daftarcommand;

import com.botline.zoe.receivers.userslists.UsersList;

public class DaftarCounterCommand extends DaftarZoeCommand {

    public DaftarCounterCommand(UsersList usersList) {
        super(usersList);
    }

    @Override
    public String toString() {
        String str = "-- Zoe's Register Counter Command --\n"
                + "Use this command by typing \"DAFTAR [nama_user] Counter\".\n"
                + "This command will register you as our counter.";

        return str;
    }
}
