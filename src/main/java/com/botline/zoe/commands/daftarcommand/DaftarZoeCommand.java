package com.botline.zoe.commands.daftarcommand;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.receivers.userslists.UsersList;

public abstract class DaftarZoeCommand implements ZoeCommand {

    UsersList usersList;

    public DaftarZoeCommand(UsersList usersList) {
        this.usersList = usersList;
    }


    @Override
    public void execute(String[] parameters, String userId) {
        this.usersList.doCommand(parameters, userId);
    }

    @Override
    public abstract String toString();
}
