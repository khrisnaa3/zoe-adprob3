package com.botline.zoe.commands.daftarcommand;

import com.botline.zoe.receivers.userslists.UsersList;

public class DaftarPembeliCommand extends DaftarZoeCommand {

    UsersList usersList;

    public DaftarPembeliCommand(UsersList usersList) {
        super(usersList);
    }

    @Override
    public String toString() {
        String str = "-- Zoe's Register Buyer Command --\n"
                + "Use this command by typing \"DAFTAR [nama_user] Pembeli\".\n"
                + "This command will register you as our Buyer.";

        return str;
    }
}
