package com.botline.zoe.commands;

import com.botline.zoe.receivers.PesanReceiver;

public class PesanCommand implements ZoeCommand {
    PesanReceiver pesanReceiver;

    public PesanCommand(PesanReceiver pesanReceiver) {
        this.pesanReceiver = pesanReceiver;
    }

    @Override
    public void execute(String[] parameters, String userId) {
        pesanReceiver.doCommand(parameters, userId);
    }


    @Override
    public String toString() {
        String str = "-- Zoe's Order Command --\n"
                + "Use this command by typing \"PESAN NAMA_COUNTER NOMOR_MENU JUMLAH\".\n"
                + "Example : pesan counterAyam 3 2";

        return str;
    }
}
