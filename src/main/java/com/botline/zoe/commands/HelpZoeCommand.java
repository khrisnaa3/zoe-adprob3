package com.botline.zoe.commands;

import com.botline.zoe.receivers.HelpZoeReceiver;

public class HelpZoeCommand implements ZoeCommand {

    HelpZoeReceiver helpZoeReceiver;

    public HelpZoeCommand(HelpZoeReceiver helpZoeReceiver) {
        this.helpZoeReceiver = helpZoeReceiver;
    }

    @Override
    public void execute(String[] parameters, String userId) {
        this.helpZoeReceiver.doCommand();
    }

    @Override
    public String toString() {
        String str = "-- Zoe's Help Command --\n"
                + "Use this command by typing \"HELP\".\n"
                + "This command will tell you the info for every command that Zoe has.\n"
                + "It will also tell you what you need to type for every command.\n"
                + "Happy Eating!!";

        return str;
    }
}


