package com.botline.zoe.controller;

import com.botline.zoe.commands.*;
import com.botline.zoe.commands.daftarcommand.DaftarCounterCommand;
import com.botline.zoe.commands.daftarcommand.DaftarPembeliCommand;
import com.botline.zoe.commands.listcommand.ListOfCounterCommand;
import com.botline.zoe.commands.listcommand.ListOfPembeliCommand;
import com.botline.zoe.invoker.ZoeInvoker;
import com.botline.zoe.receivers.*;
import com.botline.zoe.receivers.listwaitress.ListCounterWaitress;
import com.botline.zoe.receivers.listwaitress.ListPembeliWaitress;
import com.botline.zoe.receivers.userslists.CounterList;
import com.botline.zoe.receivers.userslists.PembeliList;
import com.botline.zoe.receivers.userslists.UsersList;
import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Pembeli;
import com.linecorp.bot.client.LineMessagingClient;
import java.lang.*;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

public class ZoeController {

    @Autowired
    private LineMessagingClient lineMessagingClient;
    private ZoeInvoker zoeInvoker;
    private ArrayList<ZoeCommand> zoeCommands;
    private ArrayList<ZoeReceiver> zoeReceivers;
    private HashMap<String, Integer> hm = new HashMap<String, Integer>();


    public ZoeController() {
        zoeInvoker = new ZoeInvoker();
        zoeCommands = new ArrayList<>();
        zoeReceivers = new ArrayList<>();
        createReceivers();
        createCommands();
        setCommands();
        setMap();
    }

    public HashMap<String, Integer> getMap() {
        return hm;
    }

    public ZoeInvoker getZoeInvoker() {
        return zoeInvoker;
    }

    public void setZoeInvoker(ZoeInvoker zoeInvoker) {
        this.zoeInvoker = zoeInvoker;
    }

    public ArrayList<ZoeCommand> getZoeCommands() {
        return zoeCommands;
    }

    public ArrayList<ZoeReceiver> getZoeReceivers() {
        return zoeReceivers;
    }


    public void setZoeReceivers(ArrayList<ZoeReceiver> zoeReceivers) {
        this.zoeReceivers = zoeReceivers;
    }

    public void createReceivers() {

        ZoeReceiver helpZoeReceiver = new HelpZoeReceiver(zoeInvoker);
        ZoeReceiver pembeliReceiver = new PembeliList();
        ZoeReceiver counterReceiver = new CounterList();
        ZoeReceiver tambahMenuReceiver = new TambahMenuZoeReceiver();
        ZoeReceiver menuReceiver = new MenuWaitress((UsersList) counterReceiver);
        ZoeReceiver listPembeliReceiver = new ListPembeliWaitress((PembeliList) pembeliReceiver);
        ZoeReceiver listCounterReceiver = new ListCounterWaitress((CounterList) counterReceiver);
        ZoeReceiver pesanReceiver = new PesanReceiver();

        zoeReceivers.add(helpZoeReceiver);
        zoeReceivers.add(pembeliReceiver);
        zoeReceivers.add(counterReceiver);
        zoeReceivers.add(tambahMenuReceiver);
        zoeReceivers.add(menuReceiver);
        zoeReceivers.add(listPembeliReceiver);
        zoeReceivers.add(listCounterReceiver);
        zoeReceivers.add(pesanReceiver);
        //TODO Receiver lainnya
    }

    public void createCommands() {
        HelpZoeReceiver helpZoeReceiver = (HelpZoeReceiver) zoeReceivers.get(0);
        PembeliList pembeliReceiver = (PembeliList) zoeReceivers.get(1);
        CounterList counterReceiver = (CounterList) zoeReceivers.get(2);
        TambahMenuZoeReceiver tambahMenuZoeReceiver = (TambahMenuZoeReceiver) zoeReceivers.get(3);
        MenuWaitress menuWaitress = (MenuWaitress) zoeReceivers.get(4);
        ListPembeliWaitress listPembeliWaitress = (ListPembeliWaitress) zoeReceivers.get(5);
        ListCounterWaitress listCounterWaitress = (ListCounterWaitress) zoeReceivers.get(6);
        PesanReceiver pesanReceiver = (PesanReceiver) zoeReceivers.get(7);

        ZoeCommand helpCommand = new HelpZoeCommand(helpZoeReceiver);
        ZoeCommand daftarPembeliCommand = new DaftarPembeliCommand(pembeliReceiver);
        ZoeCommand daftarCounterCommand = new DaftarCounterCommand(counterReceiver);
        ZoeCommand tambahMenuCommand = new TambahMenuZoeCommand(tambahMenuZoeReceiver);
        ZoeCommand menuCommand = new MenuZoeCommand(menuWaitress);
        ZoeCommand listPembeliCommand = new ListOfPembeliCommand(listPembeliWaitress);
        ZoeCommand listCounterCommand = new ListOfCounterCommand(listCounterWaitress);
        ZoeCommand pesanCommand = new PesanCommand(pesanReceiver);

        zoeCommands.add(helpCommand);
        zoeCommands.add(daftarPembeliCommand);
        zoeCommands.add(daftarCounterCommand);
        zoeCommands.add(tambahMenuCommand);
        zoeCommands.add(menuCommand);
        zoeCommands.add(listPembeliCommand);
        zoeCommands.add(listCounterCommand);
        zoeCommands.add(pesanCommand);
        //TODO Command lainnya
    }

    public void setCommands() {
        HelpZoeCommand helpZoeCommand = (HelpZoeCommand) zoeCommands.get(0);
        DaftarPembeliCommand daftarPembeliCommand = (DaftarPembeliCommand) zoeCommands.get(1);
        DaftarCounterCommand daftarCounterCommand = (DaftarCounterCommand) zoeCommands.get(2);
        TambahMenuZoeCommand tambahMenuZoeCommand = (TambahMenuZoeCommand) zoeCommands.get(3);
        MenuZoeCommand menuZoeCommand = (MenuZoeCommand) zoeCommands.get(4);
        ListOfPembeliCommand listOfPembeliCommand = (ListOfPembeliCommand) zoeCommands.get(5);
        ListOfCounterCommand listOfCounterCommand = (ListOfCounterCommand) zoeCommands.get(6);
        PesanCommand pesanCommand = (PesanCommand) zoeCommands.get(7);

        zoeInvoker.setCommand(helpZoeCommand);
        zoeInvoker.setCommand(daftarPembeliCommand);
        zoeInvoker.setCommand(daftarCounterCommand);
        zoeInvoker.setCommand(tambahMenuZoeCommand);
        zoeInvoker.setCommand(menuZoeCommand);
        zoeInvoker.setCommand(listOfPembeliCommand);
        zoeInvoker.setCommand(listOfCounterCommand);
        zoeInvoker.setCommand(pesanCommand);
        //TODO Set command lainnya
    }

    public void pressZoeInvoker(int index, String[] parameters, String userId) {

        zoeInvoker.buttonPressed(index, parameters, userId);
    }

    public String getReply(ZoeReceiver receiver) {
        String jawaban = receiver.getMessage();
        return jawaban;
    }

    public void tambahMenuPrepare(String[] parameter) {
        UsersList counterList = (UsersList) zoeReceivers.get(2);
        Counter findCounter = (Counter) counterList.findUser(parameter[1], "fakeId");
        TambahMenuZoeReceiver tambahMenuZoeReceiver = (TambahMenuZoeReceiver) zoeReceivers.get(3);
        tambahMenuZoeReceiver.setCounter(findCounter);
    }

    public void menuPrepare() {
        CounterList daftarCounter = (CounterList) zoeReceivers.get(2);
        MenuWaitress newMenuWaitress = new MenuWaitress(daftarCounter);
        zoeReceivers.set(4, newMenuWaitress);
        MenuZoeCommand newMenuCommand = new MenuZoeCommand(newMenuWaitress);
        zoeCommands.set(4, newMenuCommand);
        zoeInvoker.setCommand(4, newMenuCommand);
    }

    public void pembeliPrepare() {
        PembeliList daftarPembeli = (PembeliList) zoeReceivers.get(1);
        ListPembeliWaitress newListPembeli = new ListPembeliWaitress((daftarPembeli));
        zoeReceivers.set(5, newListPembeli);
        ListOfPembeliCommand newListPembeliCommand = new ListOfPembeliCommand(newListPembeli);
        zoeCommands.set(5, newListPembeliCommand);
        zoeInvoker.setCommand(5, newListPembeliCommand);
    }

    public void counterPrepare() {
        CounterList daftarCounter = (CounterList) zoeReceivers.get(2);
        ListCounterWaitress newListCounter = new ListCounterWaitress((daftarCounter));
        zoeReceivers.set(6, newListCounter);
        ListOfCounterCommand newListCounterCommand = new ListOfCounterCommand(newListCounter);
        zoeCommands.set(6, newListCounterCommand);
        zoeInvoker.setCommand(6, newListCounterCommand);

    }

    public void pesanPrepare(String[] parameter) {
        UsersList counterList = (UsersList) zoeReceivers.get(2);
        Counter findCounter = (Counter) counterList.findUser(parameter[1], "fakeId");
        PesanReceiver pesanReceiver = (PesanReceiver) zoeReceivers.get(7);
        pesanReceiver.setCounter(findCounter);
    }

    public String[] extendWithNamaPembeli(String[] parameter, String idPembeli) {
        int length = parameter.length;
        String namaPembeli = pesanFindPembeli(idPembeli);
        String result[] = new String[length + 1];
        for (int i = 0; i < length; i++) {
            result[i] = parameter[i];
        }
        result[length] = namaPembeli;
        return result;
    }

    public String pesanFindPembeli(String idPembeli) {
        UsersList pembeliList = (UsersList) zoeReceivers.get(1);
        Pembeli findPembeli = (Pembeli) pembeliList.findUser("fake name", idPembeli);
        if (findPembeli == null) {
            return "null";
        }
        return findPembeli.getName();
    }

    public void setMap() {
        this.hm.put("help", 1);
        this.hm.put("daftar", 3);
        this.hm.put("tambah_menu", 4);
        this.hm.put("menu", 1);
        this.hm.put("list_pembeli", 1);
        this.hm.put("list_counter", 1);
        this.hm.put("pesan", 4);
    }

    public int getMapValidlength(String str) {
        if (hm.containsKey(str)) {
            return hm.get(str);
        } else {
            return -1;
        }
    }

}
