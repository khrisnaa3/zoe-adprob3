package com.botline.zoe.receivers;

import com.botline.zoe.iterators.ZoeIterator;
import com.botline.zoe.menus.MenuItem;
import com.botline.zoe.receivers.userslists.UsersList;
import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Users;

import java.util.ArrayList;

public class MenuWaitress extends ZoeReceiver {

    String response;
    ArrayList<Users> listOfUsers;

    public MenuWaitress(UsersList usersList) {
        this.listOfUsers = usersList.listOfUsers;
        response = "";
    }

    public void doCommand() {
        for (int i = 0; i < listOfUsers.size(); i++) {
            Counter counter = (Counter) listOfUsers.get(i);
            ZoeIterator counterIterator = counter.createIterator();
            response += "Menu for " + counter.getName() + " counter:\n";
            response += printMenu(counterIterator) + "\n";
        }
        setMessage(response);
    }

    public String printMenu(ZoeIterator zoeIterator) {
        String result = "";
        int count = 1;
        while (zoeIterator.hasNext()) {
            MenuItem menuItem = (MenuItem) zoeIterator.next();
            result += count + ". ";
            result += menuItem.getNama() + " @";
            result += menuItem.getHarga() + "\n";
            count++;
        }
        return result;
    }
}
