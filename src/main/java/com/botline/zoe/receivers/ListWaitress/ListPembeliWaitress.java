package com.botline.zoe.receivers.listwaitress;

import com.botline.zoe.iterators.ZoeIterator;
import com.botline.zoe.receivers.ZoeReceiver;
import com.botline.zoe.receivers.userslists.PembeliList;
import com.botline.zoe.users.Pembeli;
import com.botline.zoe.users.Users;

import java.util.ArrayList;

public class ListPembeliWaitress extends ZoeReceiver {

    String response;
    PembeliList pembeliList;
    ArrayList<Users> listOfUsers;

    public ListPembeliWaitress(PembeliList pembeliList) {
        this.pembeliList = pembeliList;
        this.listOfUsers = pembeliList.listOfUsers;
        response = "";
    }

    public void doCommand() {
        ZoeIterator pembeliListIterator = pembeliList.createIterator();
        response = "List of Pembeli\n";
        int count = 1;
        while (pembeliListIterator.hasNext()) {
            Pembeli pembeli = (Pembeli) pembeliListIterator.next();
            response += count + ". ";
            response += pembeli.getName() + "\n";
            count++;
        }
        setMessage(response);
    }
}
