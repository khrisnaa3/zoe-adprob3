package com.botline.zoe.receivers.listwaitress;

import com.botline.zoe.iterators.ZoeIterator;
import com.botline.zoe.receivers.ZoeReceiver;
import com.botline.zoe.receivers.userslists.CounterList;
import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Users;

import java.util.ArrayList;

public class ListCounterWaitress extends ZoeReceiver {

    String response;
    CounterList counterList;
    ArrayList<Users> listOfUsers;

    public ListCounterWaitress(CounterList counterList) {
        this.counterList = counterList;
        this.listOfUsers = counterList.listOfUsers;
        response = "";
    }

    public void doCommand() {
        ZoeIterator counterListIterator = counterList.createIterator();
        response = "List of Counters\n";
        int count = 1;
        while (counterListIterator.hasNext()) {
            Counter counter = (Counter) counterListIterator.next();
            response += count + ". ";
            response += counter.getName() + "\n";
            count++;
        }
        setMessage(response);
    }
}
