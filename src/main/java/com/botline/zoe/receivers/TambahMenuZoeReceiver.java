package com.botline.zoe.receivers;

import com.botline.zoe.menus.MenuItem;
import com.botline.zoe.users.Counter;

import java.util.ArrayList;

public class TambahMenuZoeReceiver extends ZoeReceiver {

    Counter counter;
    MenuItem menu;
    String message;

    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    public void doCommand() {
        this.counter.addMenu(menu);
    }

    public void doCommand(String[] param, String userId) {
        if (counter == null) {
            String message = "Counter not found! :( Make sure you already "
                    + "register and type the right counter name";
            setMessage(message);
            return;
        }
        String counterId = "";

        if (counter != null) {
            counterId = counter.getUserId();
        }

        if (!counterId.equals(userId)) {
            message = "You are not the owner of this counter!"
                    + "\n Sorry :(";
            setMessage(message);
            return;
        }

        findMenu(param[2]);
        if (this.menu != null) {
            message = "I cant have duplicate menus " +
                    "for your counter!";
            setMessage(message);
            return;
        }


        try {
            MenuItem newMenu = new MenuItem(param[2], Integer.parseInt(param[3]));
            this.menu = newMenu;

            if(menu.getHarga() <= 0){
                throw new NumberFormatException();
            }

            message = "Counter " + param[1]
                    + " has added a new menu "
                    + menu.getNama()
                    + " with a price of "
                    + menu.getHarga() + ".\n"
                    + "Very delicious!!";
            doCommand();
            setMessage(message);

        } catch (NumberFormatException exception) {
            message = "Please input the price as number!";
            setMessage(message);
        }
    }

    public void findMenu(String nama) {
        ArrayList<MenuItem> menus = counter.getmenu();
        for (int i = 0; i < menus.size(); i++) {
            MenuItem current = menus.get(i);
            String curName = current.getNama();
            if (curName.equals(nama)) {
                this.menu = current;
                return;
            }
        }
        this.menu = null;
    }
}
