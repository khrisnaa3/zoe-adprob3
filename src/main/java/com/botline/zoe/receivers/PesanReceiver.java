package com.botline.zoe.receivers;

import com.botline.zoe.menus.MenuItem;
import com.botline.zoe.users.Counter;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.PushMessage;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class PesanReceiver extends ZoeReceiver {

    final LineMessagingClient client = LineMessagingClient
            .builder("2tngWRxSvxGBSypTuvNo2EvelRCRoyErh0/OCuPtEyjxSJXy6"
                    + "XMevd94PwKBk5zgSswJGILAv8+CksWUsyfP10z6VcwWwajQpUYNjOYDmzl2Ul"
                    + "vX9sRvWUvzUy0dG9fA7rNzQqWzN8lyOztXS4cDzwdB04t89/1O/w1cDnyilFU=")
            .build();

    Counter counter;
    String userIdCounter;
    ArrayList<MenuItem> counterMenus;
    String namaCounter;
    MenuItem menu;
    String parameters[];

    public void setCounter(Counter counter) {
        this.counter = counter;
        if (counter != null) {
            this.userIdCounter = counter.getUserId();
            this.counterMenus = counter.getmenu();
            this.namaCounter = counter.getName();
        }
    }

    public void doCommand(String[] param, String userIdPembeli) {
        parameters = param;
        int nomorMenu = parseNomorMenu(param[2]);
        int jumlah = parseJumlah(param[3]);
        String namaPembeli = param[4];

        if (jumlah < 1) {
            String message = "You cannot order something below zero!";
            setMessage(message);
            return;
        }
        if (counter == null) {
            String message = "I can't find the counter "
                    + "that you want to order to! :(";
            setMessage(message);
            return;
        }
        try {
            MenuItem theMenu = counterMenus.get(nomorMenu - 1);
            int price = theMenu.getHarga() * jumlah;

            String namaMenu = theMenu.getNama();
            parameters[0] = namaMenu;

            doCommand();
            messageToPembeli(price);
        } catch (IndexOutOfBoundsException exception) {
            String message = "Please input available menu number!";
            setMessage(message);
        }
    }

    public TextMessage createTextMessage() {
        final TextMessage textMessage = new TextMessage(
                "Hello " + namaCounter + "!\n"
                        + "Order up! There is an order of " + parameters[3]
                        + " " + parameters[0] + " "
                        + "from " + parameters[4] + ".\n"
                        + "Happy cooking!"
        );
        return textMessage;
    }

    @Override
    public void doCommand() {
        final BotApiResponse botApiResponse;

        TextMessage textMessage = createTextMessage();

        final PushMessage pushMessage = new PushMessage(
                userIdCounter,
                textMessage
        );

        try {
            botApiResponse = client.pushMessage(pushMessage).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return;
        }

        System.out.println(botApiResponse);
    }

    void messageToPembeli(int price) {
        String message = "Thank you for ordering!\n"
                + "The total price is " + price + ".";
        setMessage(message);
    }

    public int parseNomorMenu(String nomor) {
        try {
            return Integer.parseInt(nomor);
        } catch (NumberFormatException exception) {
            String message = "Please input nomor menu as number!";
            setMessage(message);
            return -1;
        }
    }

    public int parseJumlah(String jumlah) {
        try {
            return Integer.parseInt(jumlah);
        } catch (NumberFormatException exception) {
            String message = "Please input jumlah as number!";
            setMessage(message);
            return -1;
        }
    }
}


