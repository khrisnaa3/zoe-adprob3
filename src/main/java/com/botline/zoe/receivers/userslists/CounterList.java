package com.botline.zoe.receivers.userslists;

import com.botline.zoe.iterators.UsersListIterator;
import com.botline.zoe.iterators.ZoeIterator;
import com.botline.zoe.users.Counter;
import com.botline.zoe.users.Users;

public class CounterList extends UsersList {

    @Override
    public void doCommand(String[] parameters, String userId) {

        String theName = parameters[1];
        Users current = findUser(theName, userId);
        String currentId = "";

        if (current != null) {
            currentId = current.getUserId();
        }

        if (current == null) {
            newUser = new Counter(theName, userId);
            doCommand();
            String message = "Counter " + parameters[1]
                    + " has been successfully created.\n"
                    + "Zoe Welcomes You!\n";
            setMessage(message);
        } else if (userId.equals(currentId)) {
            String message = "You have already registered"
                    + " as a counter!\n"
                    + "Zoe can't have 2 of you right?\n";
            setMessage(message);
        } else {
            String message = "Counter " + parameters[1]
                    + " has already been listed!\n"
                    + "Please try another name! :)\n";
            setMessage(message);
        }

    }

    public ZoeIterator createIterator() {
        return new UsersListIterator(listOfUsers);
    }
}
