package com.botline.zoe.receivers.userslists;

import com.botline.zoe.iterators.UsersListIterator;
import com.botline.zoe.iterators.ZoeIterator;
import com.botline.zoe.users.Pembeli;
import com.botline.zoe.users.Users;

public class PembeliList extends UsersList {

    @Override
    public void doCommand(String[] parameters, String userId) {

        String theName = parameters[1];
        Users current = findUser(theName, userId);
        String currentId = "";

        if (current != null) {
            currentId = current.getUserId();
        }

        if (current == null) {
            newUser = new Pembeli(theName, userId);
            doCommand();
            String message = "Pembeli " + parameters[1]
                    + " has been successfully created.\n"
                    + "Welcome! Please take a look at our menu.\n";
            setMessage(message);
        } else if (userId.equals(currentId)) {
            String message = "You have already "
                    + "registered as a buyer!\n"
                    + "You can't be two buyers"
                    + " at once right?\n";
            setMessage(message);
        } else {
            String message = "Pembeli " + parameters[1]
                    + " already exist.\n"
                    + "Please try another unique name! :)\n";
            setMessage(message);
        }

    }

    public ZoeIterator createIterator() {
        return new UsersListIterator(listOfUsers);
    }
}
