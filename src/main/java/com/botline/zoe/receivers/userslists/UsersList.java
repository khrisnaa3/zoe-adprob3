package com.botline.zoe.receivers.userslists;

import com.botline.zoe.receivers.ZoeReceiver;
import com.botline.zoe.users.Users;

import java.util.ArrayList;

public abstract class UsersList extends ZoeReceiver {

    public ArrayList<Users> listOfUsers = new ArrayList<>();
    Users newUser;

    public UsersList() {

    }

    void addUsers(Users user) {
        listOfUsers.add(user);
    }

    public Users findUser(String nama, String userId) {
        for (Users search : listOfUsers) {
            String currentName = search.getName();
            String currentId = search.getUserId();
            if (userId.equals(currentId) || nama.equals(currentName)) {
                return search;
            }
        }
        return null;
    }

    @Override
    public void doCommand() {
        addUsers(newUser);
    }

    public abstract void doCommand(String[] param, String userId);

    public void setNewUser(Users user) {
        this.newUser = user;
    }

    public Users getNewUser() {
        return this.newUser;
    }
}