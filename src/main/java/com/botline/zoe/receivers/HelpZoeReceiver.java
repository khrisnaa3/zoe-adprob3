package com.botline.zoe.receivers;

import com.botline.zoe.commands.ZoeCommand;
import com.botline.zoe.invoker.ZoeInvoker;

import java.util.ArrayList;

public class HelpZoeReceiver extends ZoeReceiver {

    private ArrayList<ZoeCommand> listOfCommands;
    private ZoeInvoker invoker;
    private String str;

    public HelpZoeReceiver(ZoeInvoker zoeInv) {
        this.invoker = zoeInv;
        this.listOfCommands = zoeInv.getCommandsList();
        str = "Hi there!\n These are the commands that you can tell me to do!\n";
        this.setMessage(str);
    }

    @Override
    public void doCommand() {
        str = "Hi there!\n These are the commands that you can tell me to do!\n\n";
        this.listOfCommands = invoker.getCommandsList();
        for (int i = 0; i < listOfCommands.size(); i++) {
            ZoeCommand theCommand = listOfCommands.get(i);
            str += (i + 1) + ". ";
            str += theCommand.toString() + "\n\n";
        }
        this.setMessage(str);
    }

}
