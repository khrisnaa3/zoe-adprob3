# Zoe - Advance Programming Project B3

## Receivers - App Description

This app consists of classes that represents Zoe's command receivers. 
These are the classes that will execute each commands directly. Each command
might have its own receiver specific on what the command will do.

## Receivers - App Contents

### Classes

1. ZoeReceiver - An interface for the receiver
2. HelpZoeReceiver - A concrete receiver class for the HelpZoeCommand
3. UsersList - A class that holds the list of users

### Utilities

1. Represents the receiver of each command
2. Able to interact with other classes that may help to execute the
commands
3. Receiver acts as the classes that will hold the message that is to
be printed by the chatbot to users


## App Task Checklist

### Week 2

- [x] Week 2: Tests for ZoeReceiver class [Raihansyah Attallah]
- [x] Week 2: Implement ZoeReceiver class [Raihansyah Attallah]

Create the ZoeReceiver interface. The interface should have a message
(String) attribute, and setter getter for that String. It also has the
doCommand() method.

- [x] Week 2: Tests for HelpZoeReceiver class [Dimas Aditya]
- [x] Week 2: Implement HelpZoeReceiver class [Dimas Aditya]

Create the HelpZoeReceiver class. This class should have the invoker 
list of commands as an attribute. It is also responsible to do the
help command by iterating on that list. Calling the toString() method 
of a command.

- [x] Week 2: Test for the new list of Users holder class 
(UsersList) [Fanisya Dewi]
- [x] Week 2: Implement the class that holds Users (UsersList)
[Fanisya Dewi]

Create the UsersList class. It should hold an arraylist of users object
and it should be able to add a user to that list and also search for a
specific user (by name).


## My Notes - Users App

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1. Bisa di refactor sehingga tiap Receiver punya Adapter yang nge adapt
class line yg bisa menotify users. Jadi saat .execute() dipanggil di 
command, command lgsg manggil adapter, adapter manggil fungsi doCommand()
receivernya dan LANGSUNG dari adapter nge notify users.