package com.botline.zoe.receivers;

public abstract class ZoeReceiver {

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public abstract void doCommand();
}
