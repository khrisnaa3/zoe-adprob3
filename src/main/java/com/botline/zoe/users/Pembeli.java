package com.botline.zoe.users;

public class Pembeli extends Users {

    private String userIdPembeli;

    public Pembeli(String name, String userIdPembeli) {
        super(name);
        setPembeli(true);
        setUserId(userIdPembeli);
    }

}
