package com.botline.zoe.users;

public abstract class Users {

    private String name;
    private boolean isPembeli = false;
    private String userId;

    public Users(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPembeli() {
        return isPembeli;
    }

    public void setPembeli(boolean pembeli) {
        isPembeli = pembeli;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }
}
