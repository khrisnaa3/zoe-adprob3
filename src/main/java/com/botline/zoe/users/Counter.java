package com.botline.zoe.users;

import com.botline.zoe.iterators.MenuZoeIterator;
import com.botline.zoe.iterators.ZoeIterator;
import com.botline.zoe.menus.MenuItem;

import java.util.ArrayList;

public class Counter extends Users {

    ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
    private String userIdCounter;

    public Counter(String name, String userIdCounter) {
        super(name);
        setPembeli(false);
        setUserId(userIdCounter);
    }

    public void addMenu(MenuItem item) {
        menuItems.add(item);
    }

    public ZoeIterator createIterator() {
        return new MenuZoeIterator(menuItems);
    }

    public ArrayList getmenu() {
        return menuItems;
    }
}