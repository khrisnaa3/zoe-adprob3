# Zoe - Advance Programming Project B3

## Invoker - App Description

This app consists of classes that represents Zoe's Invoker - the class
that holds all commands. The Invoker will gave a list of Commands
that is set by Zoe in the main class.

## Invoker - App Contents

### Classes

1. Invoker - The concrete zoeInvoker class

### Utilities

1. Holds every zoeCommand, defined by Index
2. Receiver to the HelpCommand class.


## App Task Checklist

- [x] Week 1: Create Invoker class in Invoker app
- [x] Week 1: Create tests for Invoker


## My Notes - Users App

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1.