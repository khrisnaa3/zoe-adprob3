package com.botline.zoe.invoker;

import com.botline.zoe.commands.ZoeCommand;

import java.util.ArrayList;

public class ZoeInvoker {

    ArrayList<ZoeCommand> commandsList = new ArrayList<ZoeCommand>();

    public void setCommand(ZoeCommand zoeCommand) {
        commandsList.add(zoeCommand);
    }

    public void setCommand(int idx,ZoeCommand zoeCommand) {
        commandsList.set(idx, zoeCommand);
    }

    public void buttonPressed(int index, String[] parameters, String userId) {
        ZoeCommand theZoeCommand = commandsList.get(index);
        theZoeCommand.execute(parameters, userId);
    }

    public ArrayList<ZoeCommand> getCommandsList() {
        return commandsList;
    }
}
