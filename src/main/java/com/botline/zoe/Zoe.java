package com.botline.zoe;

import com.botline.zoe.controller.ZoeController;
import com.botline.zoe.receivers.ZoeReceiver;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;

import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


@SpringBootApplication
@LineMessageHandler
public class Zoe extends SpringBootServletInitializer {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    private static ZoeController zoeController;

    public static void main(String[] args) {
        zoeController = new ZoeController();
        SpringApplication.run(Zoe.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(Zoe.class);
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        TextMessageContent theMessage = messageEvent.getMessage();
        String replyToken = messageEvent.getReplyToken();
        String message = theMessage.getText();
        String messageLower = message.toLowerCase();
        String[] parameter = messageLower.split(" ");
        String userId = messageEvent.getSource().getUserId();

        if (zoeController.getMapValidlength(parameter[0]) == parameter.length) {
            // TODO Refactor this
            if (parameter[0].equals("help")) {
                zoeController.pressZoeInvoker(0, parameter, userId);
                processMessage(0, replyToken);
            } else if (parameter[0].equals("daftar")) {
                if (parameter[2].equals("pembeli")) {
                    zoeController.pressZoeInvoker(1, parameter, userId);
                    processMessage(1, replyToken);
                } else if (parameter[2].equals("counter")) {
                    zoeController.pressZoeInvoker(2, parameter, userId);
                    processMessage(2, replyToken);
                } else {
                    String response = "Zoe doesn't recognize "
                            + "your type of user!\n"
                            + "Please type \"Help\" to "
                            + "see the list of Zoe's commands.\n";
                    replyChat(replyToken, response);
                }
            } else if (parameter[0].equals("tambah_menu")) {
                zoeController.tambahMenuPrepare(parameter);
                zoeController.pressZoeInvoker(3, parameter, userId);
                processMessage(3, replyToken);
            } else if (parameter[0].equals("menu")) {
                zoeController.menuPrepare();
                zoeController.pressZoeInvoker(4, parameter, userId);
                processMessage(4, replyToken);
            } else if (parameter[0].equals("list_pembeli")) {
                zoeController.pembeliPrepare();
                zoeController.pressZoeInvoker(5, parameter, userId);
                processMessage(5, replyToken);
            } else if (parameter[0].equals("list_counter")) {
                zoeController.counterPrepare();
                zoeController.pressZoeInvoker(6, parameter, userId);
                processMessage(6, replyToken);
            } else if (parameter[0].equals("pesan")) {
                parameter = zoeController.extendWithNamaPembeli(parameter, userId);
                if (!parameter[4].equals("null")) {
                    zoeController.pesanPrepare(parameter);
                    zoeController.pressZoeInvoker(7, parameter, userId);
                    processMessage(7, replyToken);
                } else {
                    String response = "You are not registered as a buyer."
                            + "Please register first to buy from counter :)";
                    replyChat(replyToken, response);
                }
            } else {
                String response = "Zoe doesn't recognize "
                        + "your command please type \"Help\" to "
                        + "see the list of Zoe's commands.\n";
                replyChat(replyToken, response);
            }
        } else {
            String response = "Your command is invalid "
                    + "please type \"Help\" to "
                    + "see the list of Zoe's commands.\n";
            replyChat(replyToken, response);
        }
    }

    public void processMessage(int index, String token) {
        ZoeReceiver receiver = zoeController.getZoeReceivers().get(index);
        String jawaban = zoeController.getReply(receiver);
        replyChat(token, jawaban);
    }

    public void replyChat(String replyToken, String jawaban) {
        TextMessage replyTextMessage = new TextMessage(jawaban);
        try {
            lineMessagingClient.replyMessage(new ReplyMessage(replyToken, replyTextMessage)).get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Oops! There's something wrong.");
        }
    }


}