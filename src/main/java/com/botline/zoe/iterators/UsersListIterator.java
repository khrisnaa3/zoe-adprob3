package com.botline.zoe.iterators;

import com.botline.zoe.users.Users;

import java.util.ArrayList;

public class UsersListIterator implements ZoeIterator {

    ArrayList<Users> usersList;
    int position = 0;

    public UsersListIterator(ArrayList<Users> usersList) {
        this.usersList = usersList;
    }

    @Override
    public boolean hasNext() {
        if (position >= usersList.size() || usersList.get(position) == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Object next() {
        Users users = usersList.get(position);
        position += 1;
        return users;
    }

    public ArrayList<Users> getList() {
        return usersList;
    }
}
