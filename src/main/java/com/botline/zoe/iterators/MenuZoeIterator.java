package com.botline.zoe.iterators;

import com.botline.zoe.menus.MenuItem;

import java.util.ArrayList;

public class MenuZoeIterator implements ZoeIterator {

    ArrayList<MenuItem> menuItems;
    int position = 0;

    public MenuZoeIterator(ArrayList<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    @Override
    public boolean hasNext() throws IndexOutOfBoundsException {
        if (position >= menuItems.size() || menuItems.get(position) == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public Object next() {
        MenuItem menuItem = menuItems.get(position);
        position += 1;
        return menuItem;
    }
}
