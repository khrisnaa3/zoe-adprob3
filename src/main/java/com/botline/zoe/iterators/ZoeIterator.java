package com.botline.zoe.iterators;

public interface ZoeIterator {

    boolean hasNext();

    Object next();
}