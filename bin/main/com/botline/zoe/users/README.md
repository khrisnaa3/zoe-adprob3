# Zoe - Advance Programming Project B3

## Users - App Description

This app consists of classes that represents Zoe's users both "Pemebeli"
and "Counter". It is defined with the "Users" interface. Each class has
its own different implementations.

## Users - App Contents

### Classes

1. Users - The Interface
2. Pembeli - Class that represents the buyers
3. Counter - Class that represents the counters

### Utilities

1. Able to be called by other apps as "Counter" or "Pembeli"


## App Task Checklist

### Week 1
- [x] Week 1: Create Users class and its implementation
- [x] Week 1: Create Pembeli class and Counter class 
implementation
- [x] Week 1: Create tests for Users, Pembeli, and Counter class

## My Notes - Users App

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1.