# Zoe - Advance Programming Project B3

## Menus - App Description

This app consists of classes that represents Zoe's Menu item. It is the 
class that holds each menus attributes such as name and price.

## Menus - App Contents

### Classes

1. MenuItem - the concrete MenuItem class

### Utilities

1. Represents each menu item of a counter


## App Task Checklist

- [X] Week 1: Create Menu class in Menus app
- [x] Week 1: Create tests for Menus


## My Notes - Users App

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1.