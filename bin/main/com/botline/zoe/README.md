# Zoe - Advance Programming Project B3

## Project Source Code Home
This is the main code of the Zoe Chatbot program. Each folder consists of a single
app that may be further developed in the future. Tests for each Class can be 
found in Test folder with similar directories to the source codes.

## Project Developers

- Raihansyah Attallah Andrian (1706040196)
- Daniel Anderson Estefan (1706039641)
- Dimas Aditya Faiz (1706039686)
- Fanisya Dewi N. (1706039894)
- Stefanus Khrisna Aji Hardiyanto (1706074921)

## Claims

As developers, we hope that Zoe may satisfy both buyers and sellers needs.
We are very sorry if there are any inconvenience when using our chatbot Zoe.

If you wish to report any bugs on our bot that you find please contact
one of our developers directly.

## Project Task Checklist

### Week 2

- [ ] Week 2: Test for Controller class [Raihansyah Attallah]
- [ ] Week 2: Create Controller Class implementations 
[Raihansyah Attallah]

Create a ZoeMain class that acts as the main. Implement the logic to 
create all commands, all receivers, the invoker, etc.

- [ ] Week 2: Test for Zoe class (if needed) [Daniel Anderson]
- [ ] Week 2: Implement ZoeMain class in Zoe [Daniel Anderson]

Implement the ZoeMain class so that the bot can run as it should.
This is the main class that the chatbot will run. However it will call
the ZoeMain class that has the logic of running the commands.

- [ ] Week 2: Test the ZoeMain class (if needed) [All]
- [ ] Week 2: Implement the new classes in ZoeMain [All]

Implement the new classes in ZoeMain and Main classes. Zoe should be able
to do HELP and 


## My Notes - Project Source Code Home

This section is used if there are any notes regarding codes, bugs, errors, etc. 
that have not been solved or was recently found.

1.